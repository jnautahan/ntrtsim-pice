# CMake generated Testfile for 
# Source directory: /NTRTsim/env/build/jsoncpp_3515db/src
# Build directory: /NTRTsim/env/build/jsoncpp_3515db/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(lib_json)
SUBDIRS(jsontestrunner)
SUBDIRS(test_lib_json)
