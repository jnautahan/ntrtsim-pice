# CMake generated Testfile for 
# Source directory: /NTRTsim/env/build/yamlcpp_0_5_2/test
# Build directory: /NTRTsim/env/build/yamlcpp_0_5_2/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(yaml-test "/run-tests")
SUBDIRS(gmock-1.7.0)
