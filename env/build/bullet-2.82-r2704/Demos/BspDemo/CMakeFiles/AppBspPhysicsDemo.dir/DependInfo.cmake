# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/BspConverter.cpp" "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/CMakeFiles/AppBspPhysicsDemo.dir/BspConverter.o"
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/BspDemo.cpp" "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/CMakeFiles/AppBspPhysicsDemo.dir/BspDemo.o"
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/BspLoader.cpp" "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/CMakeFiles/AppBspPhysicsDemo.dir/BspLoader.o"
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/main.cpp" "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BspDemo/CMakeFiles/AppBspPhysicsDemo.dir/main.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BT_USE_DOUBLE_PRECISION"
  "USE_GRAPHICAL_BENCHMARK"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/OpenGL/CMakeFiles/OpenGLSupport.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/BulletDynamics/CMakeFiles/BulletDynamics.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/BulletCollision/CMakeFiles/BulletCollision.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/LinearMath/CMakeFiles/LinearMath.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "src"
  "Demos/OpenGL"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
