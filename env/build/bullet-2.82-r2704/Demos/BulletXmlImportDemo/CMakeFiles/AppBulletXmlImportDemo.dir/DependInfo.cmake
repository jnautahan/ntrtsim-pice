# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BulletXmlImportDemo/BulletXmlImportDemo.cpp" "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BulletXmlImportDemo/CMakeFiles/AppBulletXmlImportDemo.dir/BulletXmlImportDemo.o"
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BulletXmlImportDemo/main.cpp" "/NTRTsim/env/build/bullet-2.82-r2704/Demos/BulletXmlImportDemo/CMakeFiles/AppBulletXmlImportDemo.dir/main.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BT_USE_DOUBLE_PRECISION"
  "DESERIALIZE_SOFT_BODIES"
  "USE_GRAPHICAL_BENCHMARK"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/NTRTsim/env/build/bullet-2.82-r2704/Demos/OpenGL/CMakeFiles/OpenGLSupport.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/Extras/Serialize/BulletXmlWorldImporter/CMakeFiles/BulletXmlWorldImporter.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/Extras/Serialize/BulletWorldImporter/CMakeFiles/BulletWorldImporter.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/BulletSoftBody/CMakeFiles/BulletSoftBody.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/BulletDynamics/CMakeFiles/BulletDynamics.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/BulletCollision/CMakeFiles/BulletCollision.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/Extras/Serialize/BulletFileLoader/CMakeFiles/BulletFileLoader.dir/DependInfo.cmake"
  "/NTRTsim/env/build/bullet-2.82-r2704/src/LinearMath/CMakeFiles/LinearMath.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "src"
  "Demos/OpenGL"
  "Extras/Serialize/BulletFileLoader"
  "Extras/Serialize/BulletWorldImporter"
  "Extras/Serialize/BulletXmlWorldImporter"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
