NASA Tensegrity Robotics Toolkit -- Control edition
===================================================

Create a working directory by pulling/cloning from BitBucket with:
```
$ git clone https://jnautahan@bitbucket.org/jnautahan/ntrtsim-pice.git
```

After cloning, the bash file /ntrtsim-pice/installation.sh should be ran:
```
$ cd ntrtsim-pice
$ bash ./installation.sh
```

This should take care of all the needed libraries. Boost.Python and Boost.NumPy are automatically downloaded if these are not yet available inside the working directory.


The working directory
=====================
Inside the ntrtsim-pice are a couple of useful directories.
The one that will be used the most is the src directory, which includes all the source code of this project. Inside /ntrtsim-pice/src/dev/pice is the current source code for control of a 6-strut tensegrity structure (SUPERball) where the control is given by Python. 

The second most important directory is the build directory, which includes all the executables/libraries built using the source code. When in these directories (for example /ntrtsim-pice/build/dev), cmake and make can be run (see cmake section). 
When adapting the source code of the current control project, the code will be compiled and linked using make inside the directory /ntrtsim-pice/build/dev/pice. This will generate a library called ```PiceLibrary.so``` which can be used by Python.


CMake
=====
When adapting the code, cmake and make commands are useful to understand. The cmake command generates Makefiles from a CMakeLists.txt file, which are in the src diretory. When running make inside a directory containing a Makefile, the libraries and directories (..etc) will be linked and an executable/library will be built. When adapting to higher levels, cmake should be run at the same level in the build directory.
To illustrate:
Say I adapt a file in the directory /ntrtsim-pice/src, then the command
```
$ cmake /ntrtsim-pice/src 
```

should be ran when inside the directory /ntrtsim-pice/build.
After that, run 
```
$ make
```

in every following directory. In this case these directories are ../build/dev and ../build/dev/pice. Then all links will be correct and the final executable/library will be built correctly.



Running/adapting the code
=========================
So most of the work is done in the source and build directories. Inside the source directory (src/dev/pice) the C++ code, which performs the rollouts of the tensegrity structure, is located along with all the header files which it needs to compile. Currently this code is made into a library using CMake and the library is called ```PiceLibrary.so```. The name and the functions/classes to be exposed to Python can be chosen here using Boost.Python. 

In the build directory (build/dev/pice) is the python code and the library which the source code generates. In here, the make command should be run when the source code is adapted in order to update the library. Run the python code using Python2.7 or adapt it using whatever text editor you like. Currently the python code uses random control. Note that currently the control should be the new length of each actuator! 



Good luck!
:panda_face:

