# Import main python libraries
import numpy as np
np.random.seed(0)
import time
import timeit

## Import the NTRTLibrary and create object so we can use it: ##
import NTRTLibrary
tensObj = NTRTLibrary.PiceMain()
# Set the most important constants for the NTRTsim:
initSpringLength = 7.0					#Initial length of springs
dt = 1./600								#Delta time in seconds
constantsArray = np.array([initSpringLength, dt])
np.savetxt("constants.txt", constantsArray)



########################################################################
## FUNCTIONS: ## Define all the functions used throughout the code     #
########################################################################
#Define a cost function which maximizes displacement:
def cost_function(initPosition, finalPosition):
	return initPosition - finalPosition
	
#Compute the effective sampling size:
def eff_sampsize(weightArray):
	return (sum(weightArray)**2)/sum(weightArray**2)
	
#Adapt the control so that the length of the springs will not be smaller than 0:
#if the length is smaller than zero, revert the noise direction by adding
#twice the noise previously added
def adapt_control(controlArray, noiseArray):
	indexArray = np.where(controlArray < 0)
	controlArray[indexArray] = controlArray[indexArray] + 2*noiseArray[indexArray]

#Compute weights	
def compute_weights(costArray, wLambda):	
	return np.exp(-wLambda*(costArray-min(costArray)))					

#Update the control paramaters using weighted averages
def update_control(control, noise, weightArray):
	newControl = control + np.sum(weightArray*noise,2)/sum(weightArray)		#Sum over all rollouts
	return newControl
	
#Compute the new lengths of the actuators:
def calc_new_length(oldLength, initLength, controlArray, noiseArray, timeConst, dt):
	newLength = oldLength + dt*(-timeConst*(oldLength-initLength) + controlArray + noiseArray/np.sqrt(dt))
	assert newLength.shape == oldLength.shape
	return abs(newLength)
	
def save_data(fileName, array, comment):
	out_name = "data/"
	np.save(out_name+fileName, array)
	
def initialize_model(tensObj, boolean):
	tensObj.initialize_model(initSpringLength, dt, boolean)

########################################################################
## MAIN: ## The main code which performs time evolution and            #
##       ## updates control                                            #
########################################################################

def open_loop_control(tensObj):
	####################################################################
	## CONSTANTS: ## Set the constants to use throughout the main code #
	####################################################################
	initPosition = 0								#Initial position. !!Assumption!! Should be measured
	nSteps = 6000									#Number of time steps
	nSprings = 9									#Number of springs
	nRollouts = 10									#Number of rollouts
	nControlUpdates = 100							#Number of control updates
	wLambda = 1										#Constant for weight calculation
	lGamma = 0.01									#Time constant for length change
	
	#Initialize the tensegrity model; The 'False' command indicates that we have no live feed
	initialize_model(tensObj, True)
	
	####################################################################
	## CONTROL: ## Use the computed control for the tensegrity         #
	####################################################################
	#Allocate and initialize control:
	meanNoise = 0.0									#Mean of normally distributed noise
	sigmaNoise = 1.0								#Sigma of normally distributed noise
	control = np.zeros((nSprings, nSteps))			#Initialize as zero control
	springLength = np.zeros((nSprings,nSteps))		#Allocate preferred spring length
	springLength[:,0] = initSpringLength*np.ones(springLength[:,0].shape)
	avgDispArr = np.zeros(nControlUpdates)			#Average displacement of the rollouts
	effSS = np.zeros(nControlUpdates)				#Effective sampling size
	
	#Check-parameters:
	lengthOne = np.zeros((nControlUpdates, nSteps))	#Preffered length of spring one each rollout
	
	starttime = time.time()
	#Run everything until satisfaction:
	for controlUpdate in range(0, nControlUpdates, 1):
		avgDisp = 0.0								#Average displacement initialized
		costArray = np.zeros(nRollouts)				#Cost array	
		#Compute the noise for this rollout:
		print "Control update nr.: %i/%i" %(controlUpdate+1, nControlUpdates)
		noise = np.random.normal(meanNoise,sigmaNoise,(nSprings,nSteps,nRollouts))
		
		#Allocate position matrix only on first and last control update:
		if controlUpdate==0:
			initPosMat = np.zeros((nRollouts, 2, nSteps))
		if controlUpdate==(nControlUpdates-1):
			finPosMat = np.zeros((nRollouts, 2, nSteps))
		
		#Do time evolution nRollouts times:
		for nroll in range(0,nRollouts,1):
			#Perform the rollouts:
			for it in range(1,nSteps,1): 
				springLength[:,it] = calc_new_length(springLength[:,it-1], initSpringLength, control[:,it], noise[:,it,nroll], lGamma, dt)	
				#Save itermediate states:
				lengthOne[controlUpdate, it] = springLength[0,it]	#Pref len of first actuator
					
				#Give state per step:
				if controlUpdate==1:	
					stateArray = tensObj.steps_give_state(springLength[:,it],1)[0]					
					initPosMat[nroll, 0, it] = stateArray[0]	#x-pos
					initPosMat[nroll, 1, it] = stateArray[2]	#z-pos
				if controlUpdate==nControlUpdates-1:
					stateArray = tensObj.steps_give_state(springLength[:,it],1)[0]					
					finPosMat[nroll, 0, it] = stateArray[0]		#x-pos
					finPosMat[nroll, 1, it] = stateArray[2]		#z-pos
					
				#Give state only last step:
				if it == nSteps-1:
					finalStateArray = tensObj.steps_give_state(springLength[:,it],1)[0]
				#Do not give state:
				else:
					tensObj.steps_no_state(springLength[:,it],1)
				
			#Compute sum of displacements
			avgDisp += finalStateArray[3]
					
			#Compute the cost function for displacement:
			#Note: displacement is only in x direction!!
			costArray[nroll] = cost_function(initPosition, finalStateArray[0])
			#Reset the model:
			tensObj.reset_model()
			
		#Compute the average displacement:
		avgDispArr[controlUpdate] = avgDisp/nRollouts
		#Compute weights and effective sampling size and update control:
		weights = compute_weights(costArray, wLambda)
		effSS[controlUpdate] = eff_sampsize(weights)/nRollouts
		control = update_control(control, noise, weights)
		
			
	####################################################################
	## CLOSING: ## Save data                                           #
	####################################################################
	save_data("sampleLength", lengthOne, "Length of a spring at each time step for each rollout")
	save_data("finalControlLengths", springLength, "Saves the spring length of the very last rollout")
	save_data("finalNoise", noise, "Saves the noise used for the final rollouts")
	save_data("effSS", effSS, "Effective sampling size")
	save_data("avgdisp", avgDispArr, "Average displacement")
	save_data("initialPositions", initPosMat, "Displacement versus time for the first rollouts")
	save_data("finalPositions", finPosMat, "Displacement versus time for the last rollouts")
	
	#~ print "E_ss: ", effSS
	#~ print "Average disp: ", avgDispArr						
	timeEstimate = time.time() - starttime
	print "Estimated computation time for %i rollouts with %i time steps: " %(nRollouts, nSteps), timeEstimate, "s"
	

if __name__ == "__main__":
	open_loop_control(tensObj)	


#~ if it==0:									#Only save names/lengths once
	#~ stateLengths = outputTuple[1]			#Lengths of state vectors
	#~ stateOrder = outputTuple[2]				#String with the order of states
