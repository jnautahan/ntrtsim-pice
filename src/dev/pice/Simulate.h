#ifndef SIMULATE_H
#define SIMULATE_H

/**
 * @file Simulate.cpp
 * @brief Function definitions of Simulate
 */

#include "core/tgSimulation.h"
//~ Include C++ libraries:
#include <vector>
#include <iostream>

class PiceModel;
//~ class tgSimulation;

class Simulate
{
	public:
		
		//~ Class constructor
		Simulate();
		//~ Class destructor
		~Simulate();
		
		//~ Input state:
		void setInput(PiceModel& subject);
		//~ Simulate:
		void simulatestep(tgSimulation* simulation);
		//~ Output state:
		std::vector<double> setOutput(PiceModel& subject);	

};

#endif //SIMULATE_H
