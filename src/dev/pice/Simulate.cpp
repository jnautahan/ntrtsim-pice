#ifndef SIMULATE
#define SIMULATE

/**
 * @file Simulate.cpp
 * @brief Simulates the structure for a single step given a control and outputs the state
 */

//~ This module
#include "Simulate.h"
//~ This application
#include "PiceModel.h"
//~ Library
#include "core/tgSimulation.h"

Simulate::Simulate()
{
	std::cout << "Simulator has been constructed" << std::endl;
}

/** Simulate single step */
void Simulate::simulatestep(tgSimulation* simulation)
{	
    simulation -> run(1);   
}
#endif //SIMULATE
