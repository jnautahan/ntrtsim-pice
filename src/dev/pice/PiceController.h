#ifndef PICE_CONTROLLER_H
#define PICE_CONTROLLER_H

/**
 * @file Picecontroller.h
 * @brief Contains definitions of class PiceController
 */
// Include boost libraries
#include "boost/numpy.hpp"	// Boost.NumPy
#include "boost/python.hpp"	// Boost.Python
// Include libraries:
#include "core/tgObserver.h"
#include "LinearMath/btVector3.h"
// Include C++ libraries:
#include <vector>
#include <iostream>
#include <fstream>

//~ Forward declarations:
class PiceModel;
//~ class PrismModel;
class tgRod;
class tgBasicActuator;
class tgSpringCableActuator;


/** Controller for step-wise simulation */
//~ The tgObserver<PiceModel> makes it so that the PiceModel becomes a subject
class PiceController : public tgObserver<PiceModel>
{
	//~ Public functions:
	public:	
	
		PiceController(const double initLength);
		
		/** Nothing to delete, destructor must be virtual */
		virtual ~PiceController() { }
		
		//~ Notify observer when a setup action has occurred
		virtual void onSetup(PiceModel& subject);
		//~ Notify observer when a step action has occurred
		virtual void onStep(PiceModel& subject, double dt);
		//~ Notify observer when teardown action has occurred
		virtual void onTeardown(PiceModel& subject);	
	
		std::vector<double> initPos;	//Initial position
		double initLengths;				//Initial lenghts of actuators		
		
		/** Set control input */
		void SetControl(np::ndarray &array);
		void SetControlFromFile(const char* filename, int matSize1, int matSize2);
		
		/** Set lenght of actuators */
		void setLength(PiceModel& subject);	
		void setLength(PiceModel& subject, int step);
		
	//~ Private functions:
	private:
		// Input
		std::vector<double> input;
		std::vector<std::vector<double> > inputFromFile;
		int step = 0;
		bool fileBool;
		const double initialLength;
			
};	

#endif // PICE_CONTROLLER_H
