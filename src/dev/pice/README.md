# Open-loop controller for Tensegrity structures
The goal of the program is to train a tensegrity structure using an open-loop controller. The program generates rollouts using the C++ code from the NTRTsim. The only feedback is given at the end of the rollouts and this is used to adapt the control. The inputs the program needs are:
1. A cost function
2. An object using the NTRTLibrary, i.e. an object which can perform the rollouts using C++
3. A time scale
4. An initial length of the tensegrities actuators

## NTRTLibrary
The library used is called NTRTLibrary.so and contains the NTRTsim classes and their members. The main members of the class are in the `PiceMain.cpp` and are called by using `NTRTLibrary.PiceMain()`, which is an object. 
There are currently 5 members of the `PiceMain()` class exported to python, namely:
1. `PiceMain.initialize_model(initSpingLength, dt, boolean)`:<br />
	Initializes the model and sets the initial length of the actuators. The rollouts will be performed using time steps of size _dt_. The boolean tells the NTRTsim if the graphics should be turned on (`True`) or off (`False`).

2. `PiceMain.steps_give_state(springLengths, nSteps)`:<br />
	Performs _nSteps_ steps of size _dt_ with the desired actuator lengths set to _springLengths_. The NTRTsim will try to change the current lengths of the actuators to match the desired lengths, according to constraints. The output of this member is the state of the tensegrity structure after all steps have been performed.
	
3. `PiceMain.steps_no_state(springLengths, nSteps)`:<br />
	Similar to `PiceMain.steps_give_state`, but has no output.

4. `PiceMain.steps_given_control(springLengths, verticalMatrixShape, horizontalMatrixShape)`:<br />
	Performs a rollout with the lengths __at each time step__ given by the _springLengths_ matrix with a shape of [verticalMatrixShape x horizontalMatrixShape]. So at each time step the desired length of the actuators are predefined. In this way the rollouts (including graphical feedback!) can be done without computing any control.
	
5. `PiceMain.reset_model()`:<br />
	Recreates the world wherein the tensegrity resided. By doing this the tensegrity (and the world) are reset back to their original state. This is to perform multiple rollouts using the same initial state.
	

## Control
The open-loop controller updates control using several rollouts which result in a cost function for each rollout. The cost function can be set to anything, but is currently set so that it favors high displacements in the _x_-direction. 
```
C(x, xInit) = -abs(x-xInit)
```
where `x` is the position (of the centre of mass) of the tensegrity structure after _nSteps_ steps and `xInit` is its initial position. The control is updated in the `update_control` function, which takes as inputs the previous control, the noise used for all rollouts and the weights computed as:
```
weights = np.exp(-wLambda*(costArray-min(costArray)))
```
where `wLambda` is a constant similar to inverse temperature. This constant can (and should) be tweaked accordingly. Afterwards the control is computed as:
```
newControl = oldControl + np.sum(weightArray*noise,2)/sum(weightArray)
```
which is just the normalized sum of the weights times the noise. Since the length of the actuators should be the input for the `steps_no_state` or `steps_give_state` members, it should be computed using the control, noise and the previous lengths:
```
newLength = oldLength + dt*(-timeConst*(oldLength-initLength) + controlArray + noiseArray/np.sqrt(dt))
```
Note that this length can be smaller than 0, but since this is not physically possible I return the absolute value. <br />
All of the above constants are numpy arrays in order to decrease computation time and increase code readability.

### Constants
There are several constants used throughout the program. I will go through them one by one:
* `nSteps`: <br />
	The number of time steps for which the rollouts are performed.
* `nSprings`: <br />
	The number of actuators of the tensegrity structure. This is here in order to allocate numpy arrays with the correct sizes. Inside the C++ code are assertations which checks if the number of springs of the used model is identical to the number of springs given in the input. Otherwise there are more springs than inputs or the other way around.
* `nRollouts`: <br />
	The number of rollouts.
* `nControlUpdates`: <br />
	The number of times the control is updated using the formulas in the __Control__ section.
* `wLambda`: <br />
	The inverse temperature coefficient for computing the weights. 
* `lGamma`: <br />
	The inverse time scale for changing the length of the actuators.

## Rollouts
There are currently 3 for-loops, one for updating the control, one for each rollout (this one might be parallelized) and one final for each time step. `steps_give_state` can only be called on the last time step, so that only the final displacement is fed back to the python program. Both step members are called with _nSteps=1_ in order to keep the for-loop in python. This for-loop might also be moved to C++ if speed is an issue. Note that when state feedback is desired, the time step for-loop should be in python. 
Afterwards the model is reset using `reset_model()`.

## Statistics
Currently there are several statistics computed using the earlier defined cost function. I compute:
- Effective sampling size: <br/>
	```
	effSS = (sum(weightArray)**2)/sum(weightArray**2)
	```
- Average displacement

Also, the displacement of the rollouts at _t=0_ and _t=T_ are stored in order to check/visualize. These values are stored in order to plot using a different program called `plot_data.py`.


## Graphical feedback
When graphical feedback is desired, `graphics_open_loop.py` should be run. This reads all of its parameters from an input file generated by `open_loop.py`, and performs the rollouts with the precomputed lengths for the actuators. This is in order to visualize a certain control policy and/or change in motion resulting from control updates.


<br />
<br />
That is all for now! <br />
-- Han -- 


### Tasks
- [x] Write seperate program which performs rollouts given a certain control.
- [ ] Store lengths of each control in a text file to read for graphical viewing
- [ ] Read out initial position from the C++.
- [ ] Generate a video which uses given control and output the video for _t_ seconds.
- [ ] Explain why we use wLambda and lGamma.
