#ifndef _NPARRAYFUNCTION_H
#define _NPARRAYFUNCTION_H

/** Header file for the npArrayFunction
 * 
 * @brief Contains classes and function definitions for performing computations in C++
 * with a numpy array as input and has a numpy array as output.
 */

#include <boost/numpy.hpp>
#include <iostream>
#include <vector>

namespace np = boost::numpy;
namespace bp = boost::python;

/** Define the class and its functions */
class ArrayFunctions
{
	public:		
		//~ Convert numpy array to vector:
		std::vector<double> np_array_to_vector(np::ndarray &array);
		//~ Convert vector to numpy array:
		np::ndarray vector_to_np_array(std::vector<double>);		
};

#endif // _NPARRAYFUNCTION_H
