/**
 * @file PiceApp.cpp
 * @brief Contains the definition function main() for the Escape T6
 * application.
 * $Id$
 */

// Boost Python 
#include "boost/numpy.hpp"
#include "boost/python.hpp"
namespace np = boost::numpy;
namespace bp = boost::python;

// This application
#include "PiceModel.h"
#include "PiceController.h"
#include "Simulate.h"
//~ #include "npArrayFunction.h"

// This library
#include "core/terrain/tgBoxGround.h"
#include "models/obstacles/tgCraterShallow.h"
#include "models/obstacles/tgCraterDeep.h"
#include "core/tgModel.h"
#include "core/tgSimViewGraphics.h"
#include "core/tgSimulation.h"
#include "core/tgWorld.h"
#include "core/tgBaseRigid.h"

// Bullet Physics
#include "LinearMath/btVector3.h"

// The C++ Standard Library
#include <iostream>
#include <fstream>
#include <ctime>
#include <typeinfo>

//~ Forward declarations:
tgBoxGround *createGround();
tgWorld *createWorld();
tgSimViewGraphics *createGraphicsView(tgWorld *world);
tgSimView *createView(tgWorld *world);
//~ From header files:
void simulatestep(tgSimulation* simulation);

/**
 * Runs simulation for a single control and outputs state at each given time.
 */
void initialize()
{
	
    //~ Create the world
    tgWorld *world = createWorld();

    //~ Create the view
    //~ tgSimViewGraphics *view = createGraphicsView(world); 	//Live viewing
    tgSimView *view = createView(world);         				//No live viewing

    //~ Create the simulation
    tgSimulation* simulation = new tgSimulation(*view);

    //~ Create the model
    PiceModel* const model = new PiceModel();   
    

    
    //~ Create controller and attach it to the model    
    double initLength = 7.0;	
    PiceController* const controller = new PiceController(initLength);
    model -> attach(controller);

    //~ Add model (with controller) to simulation
    simulation -> addModel(model);
    
    //~ Create a new simulation:
	Simulate* const simulate = new Simulate();
}


void SingleStep()
{    
    //~ Simulate:    
    
    //~ for (int i=0; i<T; i++)
    //~ {
	//~ simulate -> simulatestep(simulation);		
	//~ }    
	//~ End/reset simulation
	//~ simulation -> reset();
	
    //~ delete controller;
    //Teardown is handled by delete, so that should be automatic    
}

/** Create the ground */
tgBoxGround *createGround() 
{
    // Determine the angle of the ground in radians. All 0 is flat
    const double yaw = 0.0;
    const double pitch = 0.0;
    const double roll = 0.0;
    const btVector3 eulerAngles = btVector3(yaw, pitch, roll);  // Default: (0.0, 0.0, 0.0)
    const double friction = 0.5; // Default: 0.5
    const double restitution = 0.0;  // Default: 0.0
    const btVector3 size = btVector3(10000.0, 2, 10000.0); // Default: (500.0, 1.5, 500.0)
    const btVector3 origin = btVector3(0.0, 0.0, 0.0); // Default: (0.0, 0.0, 0.0)
    const tgBoxGround::Config groundConfig(eulerAngles, friction, restitution,
                                           size, origin);
    // the world will delete this
    return new tgBoxGround(groundConfig);
}

/** Create the world */
tgWorld *createWorld() 
{
    const tgWorld::Config config(98.1); // gravity, cm/sec^2  Use this to adjust length scale of world.
    // NB: by changing the setting below from 981 to 98.1, we've
    // scaled the world length scale to decimeters not cm.

    tgBoxGround* ground = createGround();
    return new tgWorld(config, ground);
}

/** Use for displaying tensegrities in simulation */
tgSimViewGraphics *createGraphicsView(tgWorld *world)
{
    const double timestep_physics = 1.0 / 60.0 / 10.0; // Seconds
    const double timestep_graphics = 1.f /60.f; // Seconds, AKA render rate. Leave at 1/60 for real-time viewing
    return new tgSimViewGraphics(*world, timestep_physics, timestep_graphics); 
}

/** Use for trial episodes of many tensegrities in an experiment */
tgSimView *createView(tgWorld *world) 
{
    const double timestep_physics = 1.0 / 60.0 / 10.0; // Seconds
    const double timestep_graphics = 1.f /60.f; // Seconds, AKA render rate. Leave at 1/60 for real-time viewing
    return new tgSimView(*world, timestep_physics, timestep_graphics); 
}


BOOST_PYTHON_MODULE(NTRTLibrary)
{
	np::initialize();
	bp::def("initialize", initialize);
	bp::def("SingleStep", SingleStep);
}



