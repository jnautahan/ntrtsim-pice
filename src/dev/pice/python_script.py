#Import libraries:
import numpy as np
import time
import NTRTLibrary
from open_loop import OpenLoopController

controllerClass = OpenLoopController()
tensObj = NTRTLibrary.PiceMain()

controllerClass.open_loop_control(tensObj)
