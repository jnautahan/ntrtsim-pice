#include "boost/numpy.hpp"
#include "boostnumpy.hpp"
#include <math.h>
#include <vector>

/** Set the namespaces */
namespace np = boost::numpy;
namespace bp = boost::python;

/** Copy/import the numpy array and convert to vector */
// 1D case
std::vector<double> ArrayFunctions::np_array_to_vector(np::ndarray &array)
{
	// Initialize & Allocate:
	int rows = array.shape(0);							// Set number of rows
	std::vector<double> vec;							// Allocate vector
	vec.resize(rows);
	
	// Copy the np.array:
	Py_intptr_t const *strides = array.get_strides();	// Perform magic trick
	for (int i=0; i<rows; i++)
	{
		vec[i] = *reinterpret_cast<double*>(array.get_data()+i*strides[0]);
	}	
	// Return it:
	return vec;
}
/* 2D case
std::vector< std::vector<double> > ArrayFunctions::np_array_to_vector2D(np::ndarray &array)
{
	// Initialize & Allocate:
	int rows = array.shape(0);							// Set number of rows
	int cols = array.shape(1);							// Set number of colums
	std::vector< std::vector<double> > vec;				// Allocate vector
	vec.resize(rows, std::vector<double>(cols,0.0));
	
	// Copy the np.array:
	Py_intptr_t const *strides = array.get_strides();	// Perform magic trick
	for (int i=0; i<rows; i++)
	{
		for (int j=0; j<cols; j++)
		{
			vec[i][j] = *reinterpret_cast<double*>(array.get_data()+i*strides[0] + j*strides[1]);
		}
	}
	// Return it:
	return vec;
}
* */

/** Convert the vector to an np::ndarray */
// 1D case
np::ndarray ArrayFunctions::vector_to_np_array(std::vector<double> vector)
{
	int rows = vector.size();
	int cols = 1;	
	// Build the ndarray:
	bp::tuple shape = bp::make_tuple(rows,cols);
	np::ndarray output_array = np::zeros(shape, np::dtype::get_builtin<double>());
	std::copy(vector.begin(), vector.end(), reinterpret_cast<double*>(output_array.get_data()));
				
	return output_array;
}
/* 2D case
np::ndarray ArrayFunctions::vector_to_np_array2D(std::vector<std::vector<double> > vector)
{
	int rows = vector.size();
	int cols = vector[0].size();
	// Generate a 1D vector for copying to ndarray:
	std::vector<double> temp_vec;
	for (int i=0; i<rows; i++)
	{
		temp_vec.insert(temp_vec.end(), vector[i].begin(), vector[i].end());
	}
	// Build the ndarray:
	bp::tuple shape = bp::make_tuple(rows,cols);
	np::ndarray output_array = np::zeros(shape, np::dtype::get_builtin<double>());
	std::copy(temp_vec.begin(), temp_vec.end(), reinterpret_cast<double*>(output_array.get_data()));
				
	return output_array;
}
*/



