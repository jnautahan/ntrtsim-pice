#ifndef PICEMAIN_H
#define PICEMAIN_H

/**
 * @file PiceMain.hpp
 * @brief Contains the declerations for PiceMain
 * application.
 */

//~ Include boost libraries and the cpp file which can convert between numpy arrays and cpp vectors
//~ #include "boostnumpy.hpp"	// Conversion
#include "boost/numpy.hpp"	// Boost.NumPy
#include "boost/python.hpp"	// Boost.Python

//~ Add standard libraries:
#include <vector>

//~ Add NTRTsim libraries:
#include "core/terrain/tgBoxGround.h"
#include "core/tgModel.h"
#include "core/tgSimViewGraphics.h"
#include "core/tgSimulation.h"
#include "core/tgWorld.h"
#include "core/tgBaseRigid.h"

//~ Add this project:
#include "Simulate.h"
#include "PiceModel.h"
//~ #include "PrismModel.h"
#include "PiceController.h"


/** Define the main class and declare its functions */
class PiceMain
{
	public:
		//~ No con- and destructor needed, since boost.python takes care of that
		
		//~ Initialize the model:
		void initialize_model(double initLength, const double dt, bool graphics);
		
		//~ Perform single step and output the state:
		bp::tuple steps_give_state(np::ndarray &array, int steps);
		void steps_no_state(np::ndarray &array, int steps);
		bp::tuple compute_state(PiceModel* subject);
		void steps_given_control(const char* filename, int matSize1, int matSize2);
		
		// Deep-learning functions:
		void do_rollout_step(np::ndarray &array);
		bp::tuple get_state();
		np::ndarray get_spring_lengths();
		
		//~ Reset:
		void reset_model();
		
	private:
		//~ All states come here
		double initLength;
		int initializer;
		double comDisplacement;
		std::vector<double> position;
		std::vector<double> springTension;
		std::vector<double> springLengths;
		std::vector<double> comVelocity;	
		std::vector<double> rodVelocities;
		std::vector<double> rodOrientation;
		bp::tuple outputTuple;
		
		//~ 
		tgWorld* world;
		tgSimView* view;
		tgSimulation* simulation;			
		Simulate* simulate;
		PiceModel* model;
		PiceController* controller;
};


/** Boost.Python and Boost.NumPy */
BOOST_PYTHON_MODULE(NTRTLibrary)
{
	np::initialize();	
	//~ Define class and the functions to be used by python:
	bp::class_<PiceMain>("PiceMain")
		// Deep-learning functions:
		.def("initialize_model", &PiceMain::initialize_model)
		.def("do_rollout_step", &PiceMain::do_rollout_step)
		.def("get_state", &PiceMain::get_state)
		.def("get_spring_lengths", &PiceMain::get_spring_lengths)
		// Open-loop functions:
		.def("steps_give_state", &PiceMain::steps_give_state)
		.def("steps_no_state", &PiceMain::steps_no_state)
		.def("steps_given_control", &PiceMain::steps_given_control)
		// Reset model:
		.def("reset_model", &PiceMain::reset_model)
		//~ .def("select_model", &PiceMain::select_model)
	;
}

#endif // PICEMAIN_H
