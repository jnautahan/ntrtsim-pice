/*
 * Copyright © 2012, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file PiceModel.cpp
 * @brief Contains the definition of the members of the class PiceModel.
 * $Id$
 */

// This module
#include "PiceModel.h"
// This library
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
#include "core/tgSpringCableActuator.h"
#include "core/tgRod.h"
#include "core/tgBasicActuator.h"
#include "core/tgBaseRigid.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"
// The C++ Standard Library
#include <stdexcept>
#include <set>
#include <cassert>

/**
 * Anonomous namespace so we don't have to declare the config in
 * the header.
 */
namespace
{
    /**
     * Configuration parameters so they're easily accessable.
     * All parameters must be positive.
     */
    const struct Config
    {
        double density;
        double radius;
        double stiffness;
        double damping;
        double pretension;
        double triangle_length;
        double triangle_height;
        double prism_height;  
    } c =
   {
       0.2,     // density (mass / length^3)
       0.31,     // radius (length)
       1000.0,   // stiffness (mass / sec^2)
       10.0,     // damping (mass / sec)
       500.0,     // pretension (mass * length / sec^2)
       10.0,     // triangle_length (length)
       10.0,     // triangle_height (length)
       10.0,     // prism_height (length)
  };
} // namespace

PiceModel::PiceModel() :
tgModel() 
{
}

PiceModel::~PiceModel()
{
}

void PiceModel::addNodes(tgStructure& s,
                            double edge,
                            double width,
                            double height)
{
    // bottom right
    s.addNode(-edge / 2.0, 0, 0); // 1
    // bottom left
    s.addNode( edge / 2.0, 0, 0); // 2
    // bottom front
    s.addNode(0, 0, width); // 3
    // top right
    s.addNode(-edge / 2.0, height, 0); // 4
    // top left
    s.addNode( edge / 2.0, height, 0); // 5
    // top front
    s.addNode(0, height, width); // 6
}

void PiceModel::addRods(tgStructure& s)
{
    s.addPair( 0,  4, "rod");
    s.addPair( 1,  5, "rod");
    s.addPair( 2,  3, "rod");
}

void PiceModel::addMuscles(tgStructure& s)
{
    // Bottom Triangle
    s.addPair(0, 1,  "muscle");
    s.addPair(1, 2,  "muscle");
    s.addPair(2, 0,  "muscle");
    
    // Top
    s.addPair(3, 4, "muscle");
    s.addPair(4, 5,  "muscle");
    s.addPair(5, 3,  "muscle");

    //Edges
    s.addPair(0, 3, "muscle");
    s.addPair(1, 4,  "muscle");
    s.addPair(2, 5,  "muscle");
}

void PiceModel::setup(tgWorld& world)
{
    // Define the configurations of the rods and strings
    // Note that pretension is defined for this string
    const tgRod::Config rodConfig(c.radius, c.density);
    const tgSpringCableActuator::Config muscleConfig(c.stiffness, c.damping, c.pretension);
    
    // Create a structure that will hold the details of this model
    tgStructure s;
    
    // Add nodes to the structure
    addNodes(s, c.triangle_length, c.triangle_height, c.prism_height);
    
    // Add rods to the structure
    addRods(s);
    
    // Add muscles to the structure
    addMuscles(s);
    
    // Move the structure so it doesn't start in the ground
    s.move(btVector3(0, 10, 0));
    
    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    spec.addBuilder("muscle", new tgBasicActuatorInfo(muscleConfig));
    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());
    
    // Notify controllers that setup has finished.
    notifySetup();
    
    // Actually setup the children
    tgModel::setup(world);
}

void PiceModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0)
    {
        throw std::invalid_argument("dt is not positive");
    }
    else
    {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void PiceModel::onVisit(tgModelVisitor& r)
{
    // Example: m_rod->getRigidBody()->dosomething()...
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& PiceModel::getAllMuscles() const
{
    return allMuscles;
}
    
void PiceModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}

////////////////////////////////////////////////////////////////////////
//	GET STATES:	//	Measure different states of the tensegrity object //
////////////////////////////////////////////////////////////////////////
// Return the center of mass of this model
// Pre-condition: This model has 6 rods
std::vector<double> PiceModel::getBallCOM() 
{   
	// Use tgRod to find the rods and their coordinates
    std::vector <tgRod*> rods = find<tgRod>("rod");
    assert(!rods.empty());

    btVector3 ballCenterOfMass(0, 0, 0);
    double ballMass = 0.0; 
    for (std::size_t i = 0; i < rods.size(); i++) 
    {   
        const tgRod* const rod = rods[i];
        assert(rod != NULL);
        const double rodMass = rod->mass();
        const btVector3 rodCenterOfMass = rod->centerOfMass();
        ballCenterOfMass += rodCenterOfMass * rodMass;
        ballMass += rodMass;
    }

    assert(ballMass > 0.0);
    ballCenterOfMass /= ballMass;

    // Copy to the result std::vector
    std::vector<double> result(3);
    for (size_t i = 0; i < 3; ++i) 
    { 
		result[i] = ballCenterOfMass[i]; 
	}
    return result;
}

/** Get velocities of the rods as <vx, vy, vz> */
std::vector<double> PiceModel::getRodVelocities()
{
	// Use tgBaseRigid to find the rods and their velocities
	btVector3 COMvelocity(0,0,0);
	std::vector<tgBaseRigid*> rods = find<tgBaseRigid>("rod");
	std::vector<double> rodVelocities;
	
	for (int i=0; i<rods.size(); i++)
	{
		tgBaseRigid* current_rod = rods[i];
		btRigidBody* body = current_rod -> getPRigidBody();	//Find all the bodies
		btVector3 rodVelocity = body -> getLinearVelocity();//Find their velocities
		
		for (int j=0; j<3; j++)
		{
			rodVelocities.push_back(rodVelocity[j]);		//Store their velocities
		}			
	}			
	return rodVelocities;
}
		

/** Finds the tension of the springs/muscles */
std::vector<double> PiceModel::getSpringTension()
{
	// Use tgBasicActuator to find the springs and their tension
	std::vector<tgBasicActuator*> muscles = find<tgBasicActuator>("muscle");
	assert(muscles.size()!=NULL);
	int nMuscles = muscles.size();
	
	std::vector<double> tension(nMuscles);
	for (std::size_t i=0; i<nMuscles; i++)
	{
		const tgBasicActuator* current_muscle = muscles[i];
		double current_tension = current_muscle -> getTension();
		tension[i] = current_tension;
	}
	return tension;
}

/** Finds the length of the springs/muscles */
std::vector<double> PiceModel::getSpringLengths()
{
	// Use tgSpringCableActuator to find springs and their lengths
	std::vector<tgSpringCableActuator*> muscles = find<tgSpringCableActuator>("muscle");
	assert(muscles.size()!=NULL);
	int nMuscles = muscles.size();	
	std::vector<double> springLengths(nMuscles);
	
	for (int i=0; i<nMuscles; i++)
	{
		const tgSpringCableActuator* current_muscle = muscles[i];	//Select muscle
		springLengths[i] = current_muscle -> getCurrentLength();	//Store length
	}
	return springLengths;
}

/** Finds the orientation of the rods in Euler angles*/
//~ @TODO: Write without the number 3
std::vector<double> PiceModel::getRodOrientation()
{
	std::vector<tgRod*> rods = find<tgRod>("rod");		//Finds all the rods
	std::vector<double> rodOrientation;					//Store rod orientations
	int nRods = rods.size();							//Number of rods
	
	for (std::size_t i=0; i<nRods; i++)
	{
		const tgRod* const rod = rods[i];				//Select rod i
		btVector3 rodAngles = rod -> orientation();		//Get orientation of rod i
		for (int j=0; j<3; j++)
		{
			rodOrientation.push_back(rodAngles[j]);		//Store orientation of rod i
		}
	}
    // Return the angles:
    return rodOrientation;
}
	

/** Find the position of the nodes */ //WIP
/*
std::vector<double> PiceModel::getNodePositions()
{
	std::vector<tgRodInfo*> rods = find<tgRodInfo>("rod");		//Finds all the rods (?)
	
	//~ Just do first rod for now:
	std::vector<double> rodPos;
	const tgRodInfo* const rod = rods[0];
	btVector3 btrodPos = rod -> getFrom();
	for (int i=0; i<3; i++)
	{
		rodPos[i] = btrodPos[i];
		std::cout << rodPos[i] << std::endl;pa
	}
	return rodPos;	
}
*/
