/**
 * @file PiceApp.cpp
 * @brief Contains the definition function main() for the Escape T6
 * application.
 * $Id$
 */

// Boost Python 
#include "boost/numpy.hpp"
#include "boost/python.hpp"
namespace np = boost::numpy;
namespace bp = boost::python;

// This application
#include "PiceModel.h"
//~ #include "PrismModel.h"
#include "PiceController.h"
#include "Simulate.h"
#include "PiceMain.hpp"
#include "boostnumpy.hpp"

// This library
#include "core/terrain/tgBoxGround.h"
#include "core/tgModel.h"
#include "core/tgSimViewGraphics.h"
#include "core/tgSimulation.h"
#include "core/tgWorld.h"
#include "core/tgBaseRigid.h"

// Bullet Physics
#include "LinearMath/btVector3.h"

// The C++ Standard Library
#include <iostream>
#include <cmath>
#include <ctime>
#include <typeinfo>

//~ Forward declarations:
tgBoxGround* createGround();
tgWorld* createWorld();
tgSimViewGraphics* createGraphicsView(tgWorld* world, const double dt);
tgSimView* createView(tgWorld* world, const double dt);

/**
 * Runs simulation for a single control and outputs state at each given time.
 */
void PiceMain::initialize_model(double initLength, const double dt, bool graphics)
{	
	// Create the world
    world = createWorld();

    // Create the view dependant on desire graphics boolean:
    if (graphics == false)
    {
		view = createView(world, dt);       //No live viewing
	}
	else
	{
		view = createGraphicsView(world, dt); 	//Live viewing
	}
	
    // Create the simulation
    simulation = new tgSimulation(*view);

    // Create the model
    model = new PiceModel(); 
    
    // Create controller and attach it to the model 
    controller = new PiceController(initLength);
    model -> attach(controller);

    // Add model (with controller) to simulation
    simulation -> addModel(model);    
	
	std::cout << "Initialization finished" << std::endl;
	
	// Create integer to check if the model is already initialized
	initializer = 1;	
}

/** Perform steps 
 *  Either:
 * - give length/state feedback and perform single rollout step (deep-learning)
 * - perform steps and give back the state 	(closed loop)
 * - only perform steps 					(open loop)
 * - only visual output given a certain control
 */

//////////////////////////////
// Deep-learning functions: //
//////////////////////////////
void PiceMain::do_rollout_step(np::ndarray &array)
{
	// Set control input:
	controller -> SetControl(array);
	// Perform single rollout step:
	simulation -> run(1);
}

bp::tuple PiceMain::get_state()
{
	// Return the state of the tensegrity:
	return compute_state(model);
}
 
np::ndarray PiceMain::get_spring_lengths()
{
	// Get the lengths of all the springs:
	std::vector<double> springLengths = model -> getSpringLengths();
	// Convert to np.array:
	ArrayFunctions arrfunc;
	np::ndarray npspringLengths = arrfunc.vector_to_np_array(springLengths);
	// Return the lengths of the springs:
	return npspringLengths;
}


///////////////////////////////
// Open-loop functions:      //
///////////////////////////////
void PiceMain::steps_no_state(np::ndarray &array, int steps)		//Open loop
{
	// Set control input:
	controller -> SetControl(array);	
	// Run for steps:
	simulation -> run(steps);
}


bp::tuple PiceMain::steps_give_state(np::ndarray &array, int steps)	//Closed loop
{   
	// Set control input:
	controller -> SetControl(array);	
	// Run for steps:
	simulation -> run(steps);
	// Return the state of the tensegrity:	
	return compute_state(model);
}

/** Produce only visual output given a control matrix */
void PiceMain::steps_given_control(const char* filename, int matSize1, int matSize2)
{	
	// Check if simViewGraphics is turned on:
	//~ assert(typeid(view)==typeid(tgSimViewGraphics*)
	
	controller -> SetControlFromFile(filename, matSize1, matSize2);
	simulation -> run();
}



/** Compute the state of the structure */
bp::tuple PiceMain::compute_state(PiceModel* subject)
{
	// Allocate state vectors:
	std::vector<double> stateVector;			// Saves all state parameters
	std::vector<double> stateLength;			// Saves length/index of certain state
	
	/* STATES */
	// Length of the springs/actuators
	springLengths = subject -> getSpringLengths();
	stateVector.insert(stateVector.end(), springLengths.begin(), springLengths.end());
	stateLength.push_back(springLengths.size());
	
	// Tension of the springs/actuators
	springTension = subject -> getSpringTension();
	stateVector.insert(stateVector.end(), springTension.begin(), springTension.end());
	stateLength.push_back(springTension.size());
	
	// Position of COM
	position = subject -> getBallCOM();
	comDisplacement = sqrt(position[0]*position[0] + position[2]*position[2]);
	stateVector.push_back(comDisplacement);
	stateLength.push_back(1);
	
	// Orientation (in Euler angles) of the rods
	rodOrientation = subject -> getRodOrientation();
	stateVector.insert(stateVector.end(), rodOrientation.begin(), rodOrientation.end());
	stateLength.push_back(rodOrientation.size());
	
	// Velocity of the rods (as <vx, vy, vz>)
	rodVelocities = subject -> getRodVelocities();
	stateVector.insert(stateVector.end(), rodVelocities.begin(), rodVelocities.end());
	stateLength.push_back(rodVelocities.size());
	
	// Velocity of centre of mass (as sum_i<vx_i, vy_i, vz_i>)
	comVelocity.resize(0);
	for (int i=0; i<3; i++)
	{
		double tempcomVelocity = 0.0;
		for (int j=0; j<rodVelocities.size(); j+=3)
		{
			tempcomVelocity += rodVelocities[i+j];
		}
		comVelocity.push_back(tempcomVelocity);
	}
	stateVector.insert(stateVector.end(), comVelocity.begin(), comVelocity.end());
	stateLength.push_back(comVelocity.size());
		
	//~ // Measure position:
	//~ position = subject -> getBallCOM();
	//~ stateVector.insert(stateVector.end(), position.begin(), position.end());
	//~ stateLength.push_back(position.size());
	
	// Measure displacement:
	comDisplacement = sqrt(position[0]*position[0] + position[2]*position[2]);
	stateVector.push_back(comDisplacement);
	stateLength.push_back(1);
	
	// Convert all states to np.array:	
	ArrayFunctions arrfunc;
	np::ndarray npstate_array = arrfunc.vector_to_np_array(stateVector);
	np::ndarray npstateLength = arrfunc.vector_to_np_array(stateLength);
	
	//~ if(initializer == 0)
	//~ {
		//~ initializer += 1;
		//~ bp::tuple string_tuple = bp::make_tuple("springLengths", "springTension", "position", "comDisplacement", "rodOrientation", "rodVelocity", "comVelocity");
		//~ outputTuple = bp::make_tuple(npstate_array, npstateLength, string_tuple);
		//~ return outputTuple;
	//~ }
	//~ else
	//~ {
	
	outputTuple = bp::make_tuple(npstate_array);
	return outputTuple;
}

/** Reset the model */
void PiceMain::reset_model()
{		
	//Display position before reset:
	//~ std::vector<double> bPosition = model -> getBallCOM();
	//~ std::cout << "Position before reset " << "x=" << bPosition[0] << ", z=" << bPosition[2] << std::endl;
	
	//Reset simulation:
	simulation -> reset();	
	
	//Display resetted position:
	//~ std::vector<double> rPosition = model -> getBallCOM();
	//~ std::cout << "Position after reset " << "x=" << rPosition[0] << ", z=" << rPosition[2] << std::endl;
	//~ delete controller;
	// Teardown is handled by delete, so that should be automatic    
}

/** Create the ground */
tgBoxGround *createGround() 
{
    // Determine the angle of the ground in radians. All 0 is flat
    const double yaw = 0.0;
    const double pitch = 0.0;
    const double roll = 0.0;
    const btVector3 eulerAngles = btVector3(yaw, pitch, roll);  // Default: (0.0, 0.0, 0.0)
    const double friction = 0.5; // Default: 0.5
    const double restitution = 0.0;  // Default: 0.0
    const btVector3 size = btVector3(10000.0, 2, 10000.0); // Default: (500.0, 1.5, 500.0)
    const btVector3 origin = btVector3(0.0, 0.0, 0.0); // Default: (0.0, 0.0, 0.0)
    const tgBoxGround::Config groundConfig(eulerAngles, friction, restitution,
                                           size, origin);
    // the world will delete this
    return new tgBoxGround(groundConfig);
}

/** Create the world */
tgWorld *createWorld() 
{
    const tgWorld::Config config(98.1); // gravity, cm/sec^2  Use this to adjust length scale of world.
    // NB: by changing the setting below from 981 to 98.1, we've
    // scaled the world length scale to decimeters not cm.

    tgBoxGround* ground = createGround();
    return new tgWorld(config, ground);
}

/** Use for displaying tensegrities in simulation */
tgSimViewGraphics *createGraphicsView(tgWorld *world, double dt)
{
    //~ const double timestep_physics = 1.0 / 60.0 / 10.0; // Seconds
    const double timestep_graphics = 1.f /60.f; // Seconds, AKA render rate. Leave at 1/60 for real-time viewing
    return new tgSimViewGraphics(*world, dt, timestep_graphics); 
}

/** Use for trial episodes of many tensegrities in an experiment */
tgSimView *createView(tgWorld *world, const double timestep_physics) 
{
    //~ const double timestep_physics = 1.0 / 60.0 / 10.0; // Seconds
    const double timestep_graphics = 1.f /60.f; // Seconds, AKA render rate. Leave at 1/60 for real-time viewing
    return new tgSimView(*world, timestep_physics, timestep_graphics); 
}
