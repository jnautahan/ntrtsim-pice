/** 
 * @file PiceController 
 * @brief A Pice controller which performs step-wise simulation of the SUPERball
 * 
 */

// Inclusions:
// Boost.Python & Boost.NumPy
#include "boost/numpy.hpp"
#include "boost/python.hpp"
namespace np = boost::numpy;
namespace bp = boost::python;
// This module
#include "PiceController.h"
// This application:
#include "PiceModel.h"
#include "boostnumpy.hpp"
// Libraries:
#include "core/tgBasicActuator.h"
#include "core/tgSpringCableActuator.h"
#include "core/tgRod.h"
#include "LinearMath/btVector3.h"
// Standard C++ libraries:
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <numeric>
#include <cassert>

#define M_PI 3.14159265358979323846


//~ Constructor using the model subject:
PiceController::PiceController(const double initLength):
	initialLength(initLength)
{	
	std::cout << "PiceController has been constructed" << std::endl;
}

/** Initialize position */
void PiceController::onSetup(PiceModel& subject)
{
	//~ std::cout << "onSetup is called" << std::endl;
	//~ Set initial length of every muscle in the subject:
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	for (size_t i=0; i<muscles.size(); ++i)
	{
		tgBasicActuator* const currentMuscle = muscles[i];
		assert(currentMuscle != NULL);
		currentMuscle -> setControlInput(initialLength);
	}
	
	std::vector<double> springLengths = subject.getSpringLengths();
	std::cout << "Current spring length after setup: " << springLengths[0] << std::endl;
	
	std::vector<double> initialPosition = subject.getBallCOM();
}

/** Perform controls each step */
void PiceController::onStep(PiceModel& subject, double dt)
{	
	if (dt <= 0.0) //Error message
	{	
		throw std::invalid_argument("dt is not positive");
	}		
	// Control the muscles:	
	if (fileBool==true)
	{
		setLength(subject, step);
	}
	else
	{
		setLength(subject);
	}
	
	//~ std::cout<<"Length is computed"<<std::endl;
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	for (size_t i=0; i<muscles.size(); ++i)
	{
		tgBasicActuator* const currentMuscle = muscles[i];
		assert(currentMuscle != NULL);
		currentMuscle -> moveMotors(dt);
	}
	step += 1;		//Count next step
}

/** Change length of the actuators each step */
void PiceController::setLength(PiceModel& subject)
{			
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	assert(muscles.size()==input.size());	
	
	for (size_t i=0; i<muscles.size(); ++i)
	{
		tgBasicActuator* const currentActuator = muscles[i];			
		// Compute new length of actuator:		
		double newLen = input[i];
		currentActuator -> setControlInput(newLen);			
	}
}

/** Change length of the actuators each step with given control */
void PiceController::setLength(PiceModel& subject, int step)
{
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	input = inputFromFile[step];
	std::cout << step << std::endl;
	assert(muscles.size()==input.size());
	
	for (size_t i=0; i<muscles.size(); ++i)
	{
		tgBasicActuator* const currentActuator = muscles[i];			
		// Compute new length of actuator:		
		double newLen = input[i];
		currentActuator -> setControlInput(newLen);			
	}
}

/** Set control either from numpy array or from file */
void PiceController::SetControl(np::ndarray &array)
{
	fileBool = false;
	ArrayFunctions arrfunc;
	input = arrfunc.np_array_to_vector(array);	
} 

void PiceController::SetControlFromFile(const char* filename, int matSize1, int matSize2)
{
	//~ std::cout << "Getting control input from file..." << filename << std::endl;
	fileBool = true;
	std::ifstream file(filename);
	if (!file)
	{
		std::cout << "Error opening the file." << std::endl;
	}
	else
	{
		for (int i=0; i<matSize2; i++)
		{
			std::vector<double> tempVec;
			double tempDouble;
			for (int j=0; j<matSize1; j++)
			{
				file >> tempDouble;
				tempVec.push_back(tempDouble);
			}
		inputFromFile.push_back(tempVec);
		}
	}
}


/** Save data when finished (for visibility) */
void PiceController::onTeardown(PiceModel& subject)
{
	
}
/* Save data as .csv
	//~ Save displacement as .csv:
	std::vector<double> displacement_array = displacement;
	std::ofstream displacementfile;
	displacementfile.open("displacement.csv");
	int size;
	size = displacement_array.size();
	for (int i=0; i<size; i++)
	{
		displacementfile << displacement_array[i] << "\n";
	}
	displacementfile.close();
	
	//~ Save orientations as .csv:
	std::vector<std::vector<double> > orientation_matrix = orientation;
	std::ofstream orientationfile;
	orientationfile.open("orientation.csv");
	int nCols = orientation_matrix.size();
	int nRows = orientation_matrix[0].size();
	for (int iCols=0; iCols<nCols; iCols++)
	{
		for (int iRows=0; iRows<nRows; iRows++)
		{
			orientationfile << orientation_matrix[iCols][iRows] << " ";
		}
		orientationfile << "\n";
	}
	
	//~ Save tension as .csv:
	std::ofstream tensionfile;
	tensionfile.open("tension.csv");
	for (int i=0; i<tension.size(); i++)
	{
		tensionfile << tension[i] << "\n";
	}
	tensionfile.close();
	
	//~ Save current lengths as .csv:
	std::ofstream lengthfile;
	lengthfile.open("springlengths.csv");
	for (int i=0; i<springLengths.size(); i++)
	{
		lengthfile << springLengths[i] << "\n";
	}
	lengthfile.close();
	
	//~ Save velocites as .csv:
	std::ofstream velocityfile;
	velocityfile.open("rodvelocities.csv");
	for (int i=0; i<velocities.size(); i++)
	{
		for (int j=0; j<velocities[0].size(); j++)
		{
			velocityfile << velocities[i][j] << " ";
		}
		velocityfile << "\n";
	}
	
	//~ Save velocity for COM as .csv:
	std::ofstream velocityCOMfile;
	velocityCOMfile.open("velocityCOM.csv");
	for (int i=0; i<velocityCOM.size(); i++)
	{
		velocityCOMfile << velocityCOM[i] << " ";
	}
	velocityCOMfile.close();
}
*/ 
 
 
