#!bin/bash
#INSTALLATION SCRIPT FOR THE NTRTsim
#The name is currently ntrtsim-pice while it does not yet use the pice method...
#I apoligize for that if it raises confusion.

#Exit script if anything returns false
set -e

#Set working directory wherein ntrtsim-pice will be installed:
counter=0
while [ ! -d ntrtsim-pice ]; do
	cd ..
	let "counter+=1"
	if [ $counter -gt $((2)) ]; then
		echo The installation file is not in the right directory. Please put the installation in the working directory where the directory ntrtsim-pice is also located. The installation file should not be inside the directory ntrtsim-pice!
		exit
	fi
done
echo Moved to the right directory, starting the installation...
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


# Installation function:
function ifinstall {
	appname=$1
	local installbool=0
	dpkg -l $appname &> /dev/null && { installbool=1; }
	if [ $installbool = 0 ]
	then
		echo $appname needs to be installed.
		apt-get --assume-yes install $appname
	else
		echo $appname is already installed.
	fi
}

function make_directory {
	dirname=$1
	if [ ! -d $dirname ]; then
		mkdir $dirname
	fi
}

#Update -- currently the only thing that needs sudo rights
sudo apt-get --assume-yes update
apt-get install --assume-yes git cmake	#These are necessary!!
										#For some reason cmake is not installed but
										#the dpkg returns something

#Declare which libraries to install:
#~ declare -a toinstall=("freeglut3" "freeglut3-dev" "git" "python" "python-dev" "libboost-python-dev" "python-numpy" "g++" "libglib2.0-dev" "build-essential")
declare -a toinstall=("freeglut3-dev" "python" "python-dev" "libboost-python-dev" "python-numpy" "g++" "libglib2.0-dev") 
#Install libraries if they are not installed:
for program in "${toinstall[@]}"
do
	ifinstall "$program"
done

#Get  and build Boost.NumPy:
if [ -d Boost.NumPy ]; then
	echo Boost.Numpy already exists
else
	git clone https://github.com/ndarray/Boost.NumPy.git
	cd $DIR/Boost.NumPy
	if [ -d "build" ]; then
		cd build
	elseexit
		mkdir build
		cd build
	fi
	cmake $DIR/Boost.NumPy
	make
	make install
fi

#Get the NTRTsim-pice:
cd $DIR
if [ -d $DIR/ntrtsim-pice ]; then
	echo ntrtsim-pice already exists
else
	git clone https://jnautahan@bitbucket.org/jnautahan/ntrtsim-pice.git
fi
#Make:
cd $DIR/ntrtsim-pice
make_directory build
cd $DIR/ntrtsim-pice/build
if [ -e CMakeCache.txt ]; then
	rm -f CMakeCache.txt
fi
gccpath=$(which g++)
echo Running CMake for build:
cmake -DCMAKE_CXX_COMPILER=$gccpath $DIR/ntrtsim-pice/src
make
cd dev
make 
cd pice
make 
#Move the python script to the desired folder:
if [ ! -e python_script.py ]; then
	mv $DIR/ntrtsim-pice/src/dev/pice/python_script.py .
fi


cd $DIR
#Closing:
if [ ! -e installation.sh ]; then
	mv $DIR/ntrtsim-pice/installation.sh $DIR/
fi
echo Installing and building complete!
